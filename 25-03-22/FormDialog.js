import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {  SelectFormsy, FuseChipSelectFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Checkbox from '@material-ui/core/Checkbox';


const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        Create Form
      </Button>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Add Form
            </Typography>
            <Button autoFocus color="inherit" onClick={handleClose}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        
        <div className="flex">
        <TextField
          className="mb-24 mx-24 mt-32"
          label="Form Name"
          variant="outlined"
          required
          fullWidth
        />
        </div>

        <div className='flex'>
        <TextField
                  className="my-24 mx-24"
                  label="Form Description"
                  variant="outlined"
                  multiline
                  rows={5}
                  fullWidth
          />
        </div>

        <div className='mx-24'>
          <Typography>Form Enabled</Typography>
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Enable" />
        </div>
      
        <div className="flex -mx-4">
        <TextField
          className="mb-24 mx-24 mt-32"
          label="Enter Field Name or Question"
          variant="outlined"
          required
          fullWidth
        />
        <Formsy>
        <SelectFormsy
                    className="my-24 mx-24"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Please select field type</em>
                    </MenuItem>
                    <MenuItem value="aerobics">Short Answer</MenuItem>
                    <MenuItem value="boxing">Paragraph</MenuItem>
                    <MenuItem value="karate">Dropdown</MenuItem>
                    <MenuItem value="Pilates">Multi Select Dropdown</MenuItem>
                    <MenuItem value="Yoga">Checkbox</MenuItem>
                    <MenuItem value="zumba">File</MenuItem>
                    <MenuItem value="hiit">Radio</MenuItem>
                    <MenuItem value="spinning">Date</MenuItem>
                    <MenuItem value="crossfit">Time</MenuItem>
                </SelectFormsy> 
        </Formsy>

        <FormControlLabel control={<Switch />} label="required" />
        </div>
      </Dialog>
    </div>
  );
}