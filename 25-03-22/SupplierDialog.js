import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        Add Supplier
      </Button>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Add Supplier
            </Typography>
            <Button autoFocus color="inherit" onClick={handleClose}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        
        <div className="flex -mx-4">
        
        <TextField
          className="mb-24 mx-24 mt-32"
          label="Company"
          variant="outlined"
          required
          fullWidth
        />
        <TextField
        className="mb-24 mx-24 mt-32"
        label="Supplier Name"
        variant="outlined"
        required
        fullWidth
      />
        <TextField
          className="mb-24 mx-24 mt-32"
          label="VAT Number"
          variant="outlined"
          type="number"
          required
          fullWidth
        />
        </div>

        <div className='flex -mx-4'>
        <TextField
          className="my-16 mx-24 "
          label="GST Number"
          variant="outlined"
          type="number"
          required
          fullWidth
        />
        <TextField
          className="my-16 mx-24 "
          label="Email Address"
          variant="outlined"
          type="email"
          required
          fullWidth
        />
        <TextField
          className="my-16 mx-24 "
          label="Phone Number"
          variant="outlined"
          type="number"
          required
          fullWidth
        />
        </div>

        <div className="flex -mx-4">
        
        <TextField
          className="mb-24 mx-24 mt-32"
          label="Address"
          variant="outlined"
          required
          fullWidth
        />
        <TextField
          className="mb-24 mx-24 mt-32"
          label="City"
          variant="outlined"
          required
          fullWidth
        />
        <TextField
          className="mb-24 mx-24 mt-32"
          label="State"
          variant="outlined"
          required
          fullWidth
        />
        </div>

        <div className='flex -mx-4'>
        <TextField
          className="my-16 mx-24 "
          label="Postal Code"
          variant="outlined"
          type="number"
          required
          fullWidth
        />
        <TextField
          className="my-16 mx-24 "
          label="Country"
          variant="outlined"
          required
          fullWidth
        />
        </div>

        <div className='mx-32 '>
        <FormControl>
          <Typography>Status</Typography>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        defaultValue="active"
        name="radio-buttons-group"
      >
        <FormControlLabel value="active" control={<Radio />} label="Active" />
        <FormControlLabel value="inactive" control={<Radio />} label="Inactive" />
      </RadioGroup>
    </FormControl>
        </div>
    
        
      </Dialog>
    </div>
  );
}