import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}

function priceRow(price, quantity) {
  return price * quantity;
}

function createData(no, date,name,brand,price,selling,quantity ) {
  const total = priceRow(price,quantity);
  return { no,date, name,brand,price,selling,quantity,total };
}

function subtotal(items) {
  return items.map(({ total }) => total).reduce((sum, i) => sum + i, 0);
}

const rows = [
  createData(2019012909179-1,'29-January-2019', 'NG Lim','Shoes ',400,450,9),
  createData(2019012909179-2,'18-February-2019', 'NG Lim',' Shoes',400,500,25),
  createData(2019012909179-3,'23-February-2019', 'NG Lim','test123',500,550,5),
  createData(2019012909179-4,'02-March-2019', 'NG Lim','test123',50,56,2),
 ];

 const invoiceSubtotal = subtotal(rows);

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Product Code</TableCell>
            <TableCell>Date</TableCell>
            <TableCell>Supplier Name</TableCell>
            <TableCell>Product</TableCell>
            <TableCell>Product Price</TableCell>
            <TableCell>SellingPrice</TableCell>
            <TableCell>Quantity</TableCell>
            <TableCell>Total Price</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>{row.no}</TableCell>
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.brand}</TableCell>
              <TableCell>{row.price}</TableCell>
              <TableCell>{row.selling}</TableCell>
              <TableCell>{row.quantity}</TableCell>
              <TableCell>{ccyFormat(row.total)}</TableCell>
            </TableRow>
          ))}
        </TableBody>
        <TableRow>
            <TableCell rowSpan={3} />
            <TableCell colSpan={6}>Total</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal)}</TableCell>
          </TableRow>
      </Table>
    </TableContainer>
  );
}