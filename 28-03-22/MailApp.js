import FusePageCarded from '@fuse/core/FusePageCarded';
import { makeStyles } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { useState } from 'react';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles({
  layoutRoot: {},
});

function Plan() {
  const classes = useStyles();
  const [selectedTab, setSelectedTab] = useState(0);

  

  const handleTabChange = (event, value) => {
    setSelectedTab(value);
  };

  return (
    <FusePageCarded
      classes={{
        root: classes.layoutRoot,
        toolbar: 'p-0',
      }}
      header=
      {
        <div className="flex flex-col justify-center h-full font-bold p-24">
        <div className="flex items-center flex-1">
          <h1><Icon>notifications_none</Icon>  Send Notification</h1>
        </div>
        </div>
      
    }
      
      contentToolbar={
        
        <Tabs
          value={selectedTab}
          onChange={handleTabChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="off"
          className="w-full h-64"
        >
          <Tab className="h-64"  label="ALL NOTIFICATIONS"  />
          <Tab className="h-64" label="SMS NOTIFICATIONS" />
          <Tab className="h-64"  label="EMAIL NOTIFICATIONS"  />
          <Tab className="h-64" label="PUSH NOTIFICATIONS" />
        </Tabs>
        
      }
      content={
        <div className="p-24">
          {selectedTab === 0 && (
            <div>
            </div>
          )}
          {selectedTab === 1 && (
            <div>
            </div>
          )}{selectedTab === 2 && (
            <div>
            </div>
          )}
          {selectedTab === 3 && (
            <div>
            </div>
          )}
          
        </div>
      }
    />
  );
}

export default Plan;
