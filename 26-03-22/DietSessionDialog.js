import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Toolbar from '@material-ui/core/Toolbar';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        Add Session
      </Button>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}
      classes={{
        paper: 'm-24',
      }}
      fullWidth
      maxWidth="sm">
        <Toolbar className="flex w-full">
        <DialogTitle id="customized-dialog-title" onClose={handleClose} variant="subtitle1" color="inherit">
          Diet Session
        </DialogTitle>
        </Toolbar>
        <DialogContent dividers>
        <Typography className="mt-8 mx-24">Session Name :</Typography>
          <div className="flex">
          <TextField
            className="mt-8 mx-24 "
            variant="outlined"
            label="Session Name"
            required
            fullWidth
          />
          </div>
          <Typography className="mt-16 mx-24">Start Time :</Typography>
          <div className='flex'>
          <TextField
            className="mt-8 mx-24 "
            variant="outlined"
            type="time"
            required
            fullWidth
          />
          </div>
          <Typography className="mt-16 mx-24">End Time :</Typography>
          <div className='flex'>
          <TextField
            className="mt-8 mx-24 "
            variant="outlined"
            type="time"
            required
            fullWidth
          />
          </div>

          <div className='flex'>
          <FormLabel component="legend" className="mt-16 mx-24" >Status</FormLabel>
          <RadioGroup  aria-label="status" name="status1" className="mt-8 mx-24" InputLabelProps={{
                  shrink: true,
                }}>
                                <FormControlLabel
                                    value="active"
                                    control={<Radio/>}
                                    label="Active"
                                />
                                <FormControlLabel
                                    value="inactive"
                                    control={<Radio/>}
                                    label="Inactive"
                                />
                            </RadioGroup>
          </div>
          
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Save
          </Button>
          <Button autoFocus onClick={handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}