import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

const columns = [
  { id: 'no', label: 'No', minWidth: 170 },
  { id: 'member', label: 'Member ID', minWidth: 170 },
  { id: 'name', label: 'User Name', minWidth: 170 },
  { id: 'bed', label: 'Bed Time', minWidth: 170 },
  { id: 'wake', label: 'WakeUp Time', minWidth: 170 },
  
];

function createData(no,member,name,bed,wake) {
  return { no,member,name,bed,wake };
}

const rows = [
  createData(1,'CBM0000008', 'aashish_lakhani', '10:00 PM','7:00 PM'),
  createData(2,'CBM0000010', 'abhay_bhingradia', '10:00 PM','7:00 PM'),
  createData(3,'CBM0000011', 'abhinav_agarwal', '10:00 PM','7:00 PM'),
  createData(4,'CBM0000012', 'abhinay_sharma', '10:00 PM','7:00 PM'),
  createData(5,'CBM0000013', 'abhishek_agarwal', '10:00 PM','7:00 PM'),
  createData(6,'CBM0000014', 'abhishek_dalmia', '10:00 PM','7:00 PM'),
];

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
});

export default function StickyHeadTable() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5,10,20]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
}