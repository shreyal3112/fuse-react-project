import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const TAX_RATE = 0.07;

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}


function createRow(num,  membership,  amount, gst, discount, payable) {
  return { num, membership,amount, gst, discount,payable};
}

function subtotal(items) {
  return items.map(({ amount }) => amount).reduce((sum, i) => sum + i, 0);
}

function subtotal1(items) {
  return items.map(({ gst }) => gst).reduce((sum, i) => sum + i, 0);
}

function subtotal2(items) {
  return items.map(({ discount }) => discount).reduce((sum, i) => sum + i, 0);
}

function subtotal3(items) {
  return items.map(({ payable }) => payable).reduce((sum, i) => sum + i, 0);
}

const rows = [
  createRow(1,	'Life Style Kings', 69, 558139.935, 0, 3659934),
  createRow(2,'Signature Kings', 13, 156615.5175,	9890.6748, 1026987),
  createRow(3,'Life Style Queens', 4, 35074.2375, 0,	229995),
  createRow(4,	'Signature Queens', 5, 71978.78, 0, 471992),
];

const invoiceSubtotal = subtotal(rows);
const invoiceSubtotal1 = subtotal1(rows);
const invoiceSubtotal2 = subtotal2(rows);
const invoiceSubtotal3 = subtotal3(rows);

export default function SpanningTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="spanning table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Membership Name</TableCell>
            <TableCell>Total Sale</TableCell>
            <TableCell>GST</TableCell>
            <TableCell>Discount</TableCell>
            <TableCell>Sale Aount</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>{row.num}</TableCell>
              <TableCell>{row.membership}</TableCell>
              <TableCell>{ccyFormat(row.amount)}</TableCell>
              <TableCell>{ccyFormat(row.gst)}</TableCell>
              <TableCell>{ccyFormat(row.discount)}</TableCell>
              <TableCell>{ccyFormat(row.payable)}</TableCell>
            </TableRow>
          ))}

          <TableRow>
            <TableCell rowSpan={7} />
            <TableCell colSpan={1}>Total</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal)}</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal1)}</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal2)}</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal3)}</TableCell>
          </TableRow>
          
        </TableBody>
      </Table>
    </TableContainer>
  );
}