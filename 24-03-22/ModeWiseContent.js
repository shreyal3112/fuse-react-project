import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(no, date, cash, swipe, neft, gift, credit, cheque, online, dd, other) {
  return { no, date, cash, swipe, neft, gift, credit, cheque, online, dd, other};
}

const rows = [
  createData(1, 'January-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(2, 'February-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(3, 'March-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(4, 'April-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(5, 'May-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(6, 'June-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(7, 'July-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(8, 'August-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(9, 'September-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(10, 'OCtober-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(11, 'November-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
  createData(12, 'December-2022',0, 0, 0, 0, 0, 0, 0, 0, 0),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Month-Year</TableCell>
            <TableCell>Cash</TableCell>
            <TableCell>Swipe</TableCell>
            <TableCell>NEFT</TableCell>
            <TableCell>Gift Card</TableCell>
            <TableCell>Credit Card</TableCell>
            <TableCell>Cheque</TableCell>
            <TableCell>Online</TableCell>
            <TableCell>DD</TableCell>
            <TableCell>Other</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>{row.no}</TableCell>
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.cash}</TableCell>
              <TableCell>{row.swipe}</TableCell>
              <TableCell>{row.neft}</TableCell>
              <TableCell>{row.gift}</TableCell>
              <TableCell>{row.credit}</TableCell>
              <TableCell>{row.cheque}</TableCell>
              <TableCell>{row.online}</TableCell>
              <TableCell>{row.dd}</TableCell>
              <TableCell>{row.other}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}