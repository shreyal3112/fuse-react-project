import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const TAX_RATE = 0.07;

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}


function createRow(status,name, no, membership, duration,  date,payable,paid, pending) {
  return { status,name, no, membership,  duration,date, payable,paid, pending};
}


function subtotal3(items) {
  return items.map(({ payable }) => payable).reduce((sum, i) => sum + i, 0);
}

function subtotal4(items) {
  return items.map(({ paid }) => paid).reduce((sum, i) => sum + i, 0);
}

function subtotal5(items) {
  return items.map(({ pending }) => pending).reduce((sum, i) => sum + i, 0);
}

const rows = [
  createRow(1, 'Muskan Agarwal',	8905179795,	'Life Style Kings', '30 Days','11-Nov-20200',  15000,0,15000),
  createRow(2,	'Urvi Mistry',9876543210 ,	'Life Style Kings',  '365 Days', '02-Sep-2020', 45999,30500,15499),
  createRow(3,	'Aanchal Gupta',	'-',	'Life Style Kings','31 Days', '03-Sep-2020', 10000,7000,3000),
];

const invoiceSubtotal3 = subtotal3(rows);
const invoiceSubtotal4 = subtotal4(rows);
const invoiceSubtotal5 = subtotal5(rows);

export default function SpanningTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="spanning table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Member Name</TableCell>
            <TableCell>Mobile No.</TableCell>
            <TableCell>Membership Name</TableCell>
            <TableCell>Duration</TableCell>
            <TableCell>Payment Due Date</TableCell>
            <TableCell>Payable Amount</TableCell>
            <TableCell>Paid Amount</TableCell>
            <TableCell>Pending Amount</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>{row.status}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.no}</TableCell>
              <TableCell>{row.membership}</TableCell>
              <TableCell>{row.duration}</TableCell>
              <TableCell>{row.date}</TableCell>
              <TableCell>{ccyFormat(row.payable)}</TableCell>
              <TableCell>{ccyFormat(row.paid)}</TableCell>
              <TableCell>{ccyFormat(row.pending)}</TableCell>
            </TableRow>
          ))}

          <TableRow>
            <TableCell rowSpan={7} />
            <TableCell colSpan={5}>Total</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal3)}</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal4)}</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal5)}</TableCell>
          </TableRow>
          
        </TableBody>
      </Table>
    </TableContainer>
  );
}