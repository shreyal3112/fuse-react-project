import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(no, name,  startdate, enddate,starttime,endtime,attendees, web, app, sms ) {
  return { no, name, startdate, enddate,starttime,endtime,attendees, web, app, sms  };
}

const rows = [
  createData(1, 'Test', '21-Nov-2018', '21-Nov-2018','11:21 AM',	'04:21 PM',	3,'web','app','sms'),
  createData(2, 'Christmas Zumba Party',  '25-Dec-2018', '25-Dec-2018','10:00 AM','12:00 PM',0,'','',''),
  createData(3, 'test27', '15-Dec-2018', '15-Dec-2018','10:00 AM','11:00 PM',0,'','',''),
  createData(4, 'Run For Peace',  '26-Jul-2019', '26-Jul-2019','11:07 AM','11:07 AM	',1,'web','','sms'),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Event Name</TableCell>
            <TableCell>Start Date</TableCell>
            <TableCell>End Date</TableCell>
            <TableCell>Start Time</TableCell>
            <TableCell>End Time</TableCell>
            <TableCell>Attendees</TableCell>
            <TableCell>Web</TableCell>
            <TableCell>App</TableCell>
            <TableCell>Send Sms To Eligible Member</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>
                {row.no}
              </TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.startdate}</TableCell>
              <TableCell>{row.enddate}</TableCell>
              <TableCell>{row.starttime}</TableCell>
              <TableCell>{row.endtime}</TableCell>
              <TableCell align="center">{row.attendees}</TableCell>
              <TableCell align="center">{row.web ? (
                        <Icon className="text-green text-20">check_circle</Icon>
                      ) : (
                        <Icon className="text-red text-20">remove_circle</Icon>
                      )}
              </TableCell>
              <TableCell align="center">{row.app ? (
                        <Icon className="text-green text-20">check_circle</Icon>
                      ) : (
                        <Icon className="text-red text-20">remove_circle</Icon>
                      )}</TableCell>
              <TableCell align="center">{row.sms ? (
                        <Icon className="text-green text-20">check_circle</Icon>
                      ) : (
                        <Icon className="text-red text-20">remove_circle</Icon>
                      )}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}