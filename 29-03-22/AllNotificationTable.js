import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(send, calories, fat, carbs, protein) {
  return { send, calories, fat, carbs, protein };
}

const rows = [
  createData('USER_PACKAGE_EXPIRE','email',	'Abhinay Sharma','Delivered','21 Mar 2022 , 05:10 AM' ),
  createData('USER_PACKAGE_EXPIRE','email',	'Abhinav Agarwal','Delivered',	'21 Mar 2022 , 05:10 AM' ),
  createData('USER_PAYMENT','email','Abhinay Sharma','Delivered','21 Mar 2022 , 03:40 AM' ),
  createData('USER_ATTENDANCE','email','Abhinav Agarwal','Delivered',	'02 Mar 2022 , 08:19 AM' ),
  ];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Type</TableCell>
            <TableCell>Channel</TableCell>
            <TableCell>User Name</TableCell>
            <TableCell>Status</TableCell>
            <TableCell>Send At</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.send}>
              <TableCell component="th" scope="row">
                {row.send}
              </TableCell>
              <TableCell>{row.calories}</TableCell>
              <TableCell>{row.fat}</TableCell>
              <TableCell>{row.carbs}</TableCell>
              <TableCell >{row.protein}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}