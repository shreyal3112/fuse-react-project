import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(no, name,  startdate, enddate,starttime,endtime,attendees,stage) {
  return { no, name, startdate, enddate,starttime,endtime,attendees,stage};
}

const rows = [
  createData(1, 'JOHN DOE',	9874563210,'john@doe.com','2019-07-19','01:05 pm','Walk in','Ajay Boble',	'INQUIRY MANAGEMENT'),
  createData(2, 'SANDEEP KUMAR JASSAL',9815502203	,'livelifemore@gmail.com'	,'2019-05-09','11:17 am	','Walk in',	'Ahmed Motiwala','POS/PAYMENTS'),
  createData(3, 'SHERLOCK HOLMS',	9696969696,'sherlock@mail.com	','2019-01-25,' ,'08:54 am'	,'Walk in',	'Ahmed Motiwala',	'QUALIFIED FOR FOLLOW UPS'),
  createData(4, 'SANA KHAN',	9876543212,	'sanakhan@gmail.com',	'2018-11-19', '09:59 am',	'Walk in',	'Adan Brown	','POS/PAYMENTS'),
  createData(5, 'YUG PATEL',	7878449918,'yugtest@gmail.com',	'2018-11-05', '09:35 am',	'Walk in','Deepika Jalan',	'POS/PAYMENTS'),
  createData(6, 'PURVI PATEL',	9408625019,'purvi.kintu@gmail.com',	'2018-11-03', '03:38 pm'	,'Walk in	','Kaustubh Thakor',	'QUALIFIED FOR FOLLOW UPS'),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>User Name</TableCell>
            <TableCell>Phone Number</TableCell>
            <TableCell>Email Address</TableCell>
            <TableCell>Inquiry Date</TableCell>
            <TableCell>Inquiry Type</TableCell>
            <TableCell>Assigned Employee</TableCell>
            <TableCell>Deal Stage</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>
                {row.no}
              </TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.startdate}</TableCell>
              <TableCell>{row.enddate}</TableCell>
              <TableCell>{row.starttime}</TableCell>
              <TableCell>{row.endtime}</TableCell>
              <TableCell align='center'>{row.attendees}</TableCell>
              <TableCell>{row.stage}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}