import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(no, name,  startdate, enddate,starttime,endtime,attendees,stage) {
  return { no, name, startdate, enddate,starttime,endtime,attendees,stage};
}

const rows = [
  createData(1, 'AHMED MOTIWALA','SANDEEP KUMAR JASSAL',	'POS/PAYMENTS',	'30-08-2019',	'30-08-2019','','OPEN'),
  createData(2, 'AJAY BOBLE	','JOHN DOE	','INQUIRY MANAGEMENT	','22-07-2019 ','22-07-2019',	'25-07-2019','OPEN'),
  createData(3, 'AHMED MOTIWALA',	'SHERLOCK HOLMS',	'QUALIFIED FOR FOLLOW UPS',	'25-01-2019',	'25-01-2019	','','OPEN'),
  createData(4, 'ADANBROWN27@GMAIL.COM',	'TEST TEST',	'GYM VISIT	','28-12-2018	','20-02-2020	','26-02-2020','OPEN'),
  createData(5, 'ADAN BROWN','SANA KHAN',	'POS/PAYMENTS','19-11-2018	','15-12-2018','20-06-2019','CLOSED'),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Assigned By</TableCell>
            <TableCell>User Name</TableCell>
            <TableCell>Deal Stage</TableCell>
            <TableCell>Create Date</TableCell>
            <TableCell>Close Date</TableCell>
            <TableCell>Next Followup Date</TableCell>
            <TableCell>Deal Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>
                {row.no}
              </TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.startdate}</TableCell>
              <TableCell>{row.enddate}</TableCell>
              <TableCell>{row.starttime}</TableCell>
              <TableCell>{row.endtime}</TableCell>
              <TableCell align='center'>{row.attendees}</TableCell>
              <TableCell>{row.stage}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}