import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(no, name,  startdate, enddate,starttime,endtime,attendees,part, web ) {
  return { no, name, startdate, enddate,starttime,endtime,attendees, part,web };
}

const rows = [
  createData(1, 'Group-Ex',	'Daily',	'Mon,Tue,Wed,Thur,Fri,Sat',	'04:00 PM - 05:00 PM'	,'Adan Brown'	,	'','1 / 25','web'),
  createData(2, 'Swimming',	'Daily'	,'Mon,Tue,Wed,Thur,Fri,Sat,',	'08:00 AM - 09:00 AM','Adan Brown',	'Hitesh J Patel',	'0 / 20','web'),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Class Name</TableCell>
            <TableCell>Date</TableCell>
            <TableCell>Days</TableCell>
            <TableCell>Time</TableCell>
            <TableCell>Traineer</TableCell>
            <TableCell>Proxy Traineer</TableCell>
            <TableCell>Participants</TableCell>
            <TableCell>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>
                {row.no}
              </TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.startdate}</TableCell>
              <TableCell>{row.enddate}</TableCell>
              <TableCell>{row.starttime}</TableCell>
              <TableCell>{row.endtime}</TableCell>
              <TableCell align="center">{row.attendees}</TableCell>
              <TableCell align="center">{row.part}</TableCell>
              <TableCell align="center">{row.web ? (
                        <Icon className="text-green text-20">check_circle</Icon>
                      ) : (
                        <Icon className="text-red text-20">remove_circle</Icon>
                      )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}