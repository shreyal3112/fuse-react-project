import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {  SelectFormsy, FuseChipSelectFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Radio from '@material-ui/core/Radio';
import FormLabel from '@material-ui/core/FormLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';


const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        New Inquiry
      </Button>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Add Event
            </Typography>
            <Button autoFocus color="inherit" onClick={handleClose}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        
        <div className="flex -mx-4">
        <TextField
          className="mb-24 mx-24 mt-32"
          label="First Name"
          variant="outlined"
          required
          fullWidth
        />
        
        <TextField
          className="mb-24 mx-24 mt-32"
          variant="outlined"
          label='Last Name'
          required
          fullWidth
        />

        <TextField
          className="mb-24 mx-24 mt-32"
          variant="outlined"
          label="Phone"
          required
          fullWidth
        />
        </div>

        <div className="flex -mx-4">
        <TextField
          className="mb-24 mx-24 mt-16"
          label="Email Address"
          type="email"
          variant="outlined"
          required
          fullWidth
        />
        
        <TextField
          className="mb-24 mx-16 mt-16"
          variant="outlined"
          label='Age'
          type="number"
          required
          fullWidth
        />

        <FormLabel component="legend">Gender</FormLabel>
        <FormControlLabel value="female" control={<Radio />} label="Female" />
        <FormControlLabel value="male" control={<Radio />} label="Male" />
        <FormControlLabel value="other" control={<Radio />} label="Other"/>
        </div>

        <div className="flex -mx-4">
          <Formsy>
          <SelectFormsy
                    className="mb-24 mx-24 mt-16"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em className='mx-32'>Attended By</em>
                    </MenuItem>
                    <MenuItem value="aerobics">Aerobics</MenuItem>
                    <MenuItem value="boxing">Boxing</MenuItem>
                    <MenuItem value="karate">Karate</MenuItem>
                    <MenuItem value="Pilates">Pilates</MenuItem>
                    <MenuItem value="Yoga">Yoga</MenuItem>
                    <MenuItem value="zumba">Zumba</MenuItem>
                    <MenuItem value="hiit">HIIT</MenuItem>
                    <MenuItem value="spinning">Spinning</MenuItem>
                    <MenuItem value="crossfit">Crossfit</MenuItem>
                    <MenuItem value="training">Functional Training</MenuItem>
                    <MenuItem value="workouts">Abs Workouts</MenuItem>
                    <MenuItem value="marathon">Marathon</MenuItem>
                </SelectFormsy> 
          </Formsy>
        <TextField
          className="mb-24 mx-24 mt-16"
          label="Area of City of Member"
          variant="outlined"
          required
          fullWidth
        />

<Formsy>
        <SelectFormsy
                    className="mb-24 mx-24 mt-16"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em className='mx-32' align='right'>Refrence</em>
                    </MenuItem>
                    <MenuItem value="aerobics">Hoarding</MenuItem>
                    <MenuItem value="boxing">Google</MenuItem>
                    <MenuItem value="karate">Facebook</MenuItem>
                    <MenuItem value="Pilates">Newspaper Advertisement</MenuItem>
                    <MenuItem value="Yoga">Telecalling</MenuItem>
                    <MenuItem value="zumba">Website Inquiry</MenuItem>
                    <MenuItem value="hiit">Whatsapp</MenuItem>
                    <MenuItem value="spinning">By Friends</MenuItem>
                    <MenuItem value="crossfit">Walk in</MenuItem>
                    <MenuItem value="training">Flyer</MenuItem>
                    <MenuItem value="workouts">Word of mouth</MenuItem>
                    <MenuItem value="marathon">Advertisement</MenuItem>
                    <MenuItem value="training">SMS</MenuItem>
                    <MenuItem value="workouts">Refrence</MenuItem>
                    <MenuItem value="marathon">3rd Party Website</MenuItem>
                    <MenuItem value="training">Other</MenuItem>
                </SelectFormsy> 
        </Formsy>

        <FormLabel component="legend">Status</FormLabel>
        <FormControlLabel value="pending" control={<Radio />} label="Pending" />
        <FormControlLabel value="converted" control={<Radio />} label="Converted" />
        </div>

        <Typography className="mt-16 mx-24 mb-16">Fitness Goal</Typography>
        <div className="flex -mx-24">
        <FormGroup row className='mx-64'>
        <FormGroup column>
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Improve general Health" />
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Improve muscle tone" />
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Increase Strength & power" />
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Rehabilitation Inquiry" />
        </FormGroup>
        <FormGroup column>
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Lose weight / body fat " />
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Gain weight / muscle" />
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Reduce Stress" />
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Other" />
        </FormGroup>
        <FormGroup column>
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Improve fitness " />
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Improve flexibility" />
        <FormControlLabel control={<Checkbox name="checkedC" />} label="Improve performance" />
        </FormGroup>
        </FormGroup>
        </div>

        <Typography className="mt-24 mx-24 mb-16">From when you intend to join us</Typography>
        <div className="flex -mx-4">
        <TextField
          className="mb-24 mx-24"
          variant="outlined"
          type="date"
          required
          fullWidth
        />

        <TextField
          className="mb-24 mr-24"
          variant="outlined"
          label="Anything you want to share regarding your health"
          fullWidth
        />
        </div>

        <Typography className="mt-24 mx-24 mb-16">When Is The Best Time To Call You</Typography>
        <div  className="flex -mx-8">
        <TextField
          className="mb-24 mx-24"
          type="time"
          variant="outlined"
          required
          fullWidth
        />

        <TextField
                  className="mb-24 mx-24"
                  label="Description"
                  variant="outlined"
                  multiline
                  rows={5}
                  fullWidth
          />
        </div>

        
        
      </Dialog>
    </div>
  );
}